///////////////////////////////////////////////////////////////////////////////////IMPORTS///////////////////////////////////////////////////////////////////////////////////


//Import ky to treat API requests
import ky from 'ky-universal'


///////////////////////////////////////////////////////////////////////////////////TYPES DECLARATIONS///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//Pokemon url and name
type IdvPokemonUrl = {

    name: string
    url: string
    
};

///////////////////////////////////////////////////////////////////////////////////FETCH///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//API url to get first 50 pokemons
const pokemonsUrl:string = "https://pokeapi.co/api/v2/pokemon?limit=50"

//Request to get a pokemon or multiple pokemons through url
//Must return an array of pokemons with their url and name
const pokemonData = async (url: string):Promise<IdvPokemonUrl[]> => {

    //Fetch request
    const data:any = await ky.get(url).json();

    //Return the requested data
    return data.results

}

///////////////////////////////////////////////////////////////////////////////////FETCH INFORMATION OF FIRST 50 POKEMONS///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Request to fetch individual pokemons information
const pokeDetails = async (url:string):Promise<any> => {

    //Fetch pokemons
     const idvPokemons = await pokemonData(url)

     //Go through all the pokemons and their data using the url
     const allData = idvPokemons.map(async (pokemon:any) => {
         const allPokemons = await ky.get(pokemon.url).json()
         return allPokemons
     })

     return Promise.all(allData)

}

//Return pokemon map with pokemon inside types
async function getPokemonMap():Promise<any>{

    //fetch pokemon information
    const data = await pokeDetails(pokemonsUrl)

    //Create the map
    const pokeMap = new Map()

    //Go through all the pokemons and set the type with the pokemon
    for await (const element of data) {
        element.types.map(async (type:any) => {

            pokeMap.has(type.type.name) != true  ? pokeMap.set(type.type.name, [element.name]) 
            :  pokeMap.set(type.type.name, [ ...pokeMap.get(type.type.name), element.name])
            
        })
    }
    
    return pokeMap 
    
} 

getPokemonMap().then((data) => {console.log(data)})